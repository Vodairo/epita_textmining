from random import randint
from sys import argv

def randomWordSize():
	actualRand = randint(0, 15)
	if actualRand <= 5:
		return 3
	else:
		if actualRand <= 9:
			return 4
		else:
			if actualRand <= 12:
				return 5
			else:
				if actualRand <= 14:
					return 6
				else:
					return 7

def randomWord(size):
	result = ""
	for i in range(0, size):
		actualRand = randint(0, 35)
		if actualRand <= 25:
			result += chr(97 + actualRand)
		else:
			result += chr(22 + actualRand)

	return result


if len(argv) == 2:
	value = 3000
	f = open(argv[1], 'w+')

	while value > 0:
		actualRand = randint(0, value)
		if actualRand < 30:
			f.write("approx 2 " + randomWord(randomWordSize()) + "\n")
			value -= 100
		else:
			if actualRand < 300:
				f.write("approx 1 " + randomWord(randomWordSize()) + "\n")
				value -= 10
			else:
				f.write("approx 0 " + randomWord(randomWordSize()) + "\n")
				value -= 1

	f.close()

else:
	if len(argv) == 3:
		f = open(argv[1], 'w+')
	
		value = 3000 / pow(10, int(argv[2]))
		while value > 0:
			f.write("approx " + argv[2][0] + " " + randomWord(randomWordSize()) + "\n")
			value -= 1
		f.close()



