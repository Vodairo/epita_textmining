======= README ========

- Proramme en JAVA 8
- Compilation du programme avec ant : le programme produit 2 archives jar :
  * TextMiningCompiler.jar
  * TextMiningApp.jar
- Documentation du code source avec Javadoc : ant doc pour la générer.

TextMiningCompiler :
  - Construction de l'arbre à l'aide d'un Patricia Trie
  - Organisation automatique de l'arbre par ordre alphabetique lors de l'ajout pour faciliter la recherche
  - Ecriture dans un fichier en suivant la syntaxe suivante : 
      * nodeValue:nodeOccurrence;offsetToNextBrother[sons]
  - L'offset est utilisé pour pouvoir passer des parties du dictionnaire lors de la lecture

TextMiningApp :
  - Chargement du dictionnaire avec la fonction map de la classe FileChanel.
  - Recherche effectuée avec un parcours profondeur.
  - Ajout dans la liste des résultats triés avec une recherche de l'index dichotomique.
  - Calcul de la distance de Damerau-Levenshtein avec une table des distances unique par recherche.


====== QUESTIONS ======

1. Il n'y a qu'un seul type de commande poosible pour TextMiningApp : approximation
   avec la commande : approx "distance" "mot". Toute autre commande est rejetée.
2. Générateur automatique de listes de commandes devant s'exécuter en moins d'une seconde en suivant les contraintes du sujet. Le programme python est disponible dans le sous dossier "test", et s'utilise de la façon suivante :
      - python samplegenerator.py (newSampleName) [distance]
3. Oui, quand certains mots sont très longs ou utilisant des caractères rares, il se peut qu'aucun résultat ne corresponde.
4. Etant donné qu'il faut effectuer une recherche approximative, le Trie est la structure de donnée indiquée. Afin d'économiser la consommation en RAM, nous avons choisi d'implémenter un Patricia Trie.
5. Un réglage automatique possible serait de faire dépendre la distance à la longueur de la chaine de caractères en entrée. Le ratio serait d'environ une distance de 1 pour tous les 4 caractères avec au minimum une distance de 1.
6. Afin d'améliorer les performances, une solution serait de paralléliser le parcours du Patricia Trie.
