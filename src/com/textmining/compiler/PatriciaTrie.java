package com.textmining.compiler;

import com.textmining.Node;
import com.textmining.NodeWrapper;
import com.textmining.Pair;
import com.textmining.TMString;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;

/**
 * PatriciaTrie class
 */
public class PatriciaTrie {
    /**
     * Build a dictionary tree using the Patricia Trie algorithm
     * @param tree The built tree
     * @param br The buffer to read the file used to build the tree
     * @return The tree in the parameters
     */
    public static NodeWrapper treeBuilder(NodeWrapper tree, BufferedReader br) throws Exception {
        String line;
        while ((line = br.readLine()) != null) {
            Pair<TMString, Integer> split = new TMString(line.toCharArray()).split('\t');

            char actualChar = split.FFirst.popFirstChar();

            Node actualNode = tree.getPage(actualChar);

            if (split.FFirst.getFValue().length == 0) {
                actualNode.setFOccurrence(split.FSecond);
            } else {
                while (true) {
                    Pair<Node, Integer> n = actualNode.searchInSons(split.FFirst);
                    if (n == null) {
                        actualNode.addSon(new Node(split.FFirst, split.FSecond));
                        break;
                    } else {
                        if (n.FSecond < n.FFirst.getFValue().length()) {
                            n.FFirst.split(n.FSecond);
                            if (n.FFirst.getFValue().length() == split.FFirst.length()) {
                                n.FFirst.setFOccurrence(split.FSecond);
                            } else {
                                n.FFirst.addSon(new Node(split.FFirst.substring(n.FSecond), split.FSecond));
                            }
                            break;
                        } else if (n.FSecond == n.FFirst.getFValue().length() && n.FSecond == split.FFirst.length()) {
                            n.FFirst.setFOccurrence(split.FSecond);
                            break;
                        } else {
                            split.FFirst = split.FFirst.substring(n.FSecond);
                            actualNode = n.FFirst;
                        }
                    }
                }
            }
        }

        return tree;
    }
}
