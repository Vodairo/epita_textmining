package com.textmining.compiler;

import com.textmining.Node;
import com.textmining.NodeWrapper;
import com.textmining.TMString;

import java.io.*;
import java.util.stream.StreamSupport;

/**
 * Created by Voda on 15/04/2015.
 */
public class Main {
    // ===========
    // Constants
    // ===========

    public final static String cacheDir = "dic";

    // ===========
    // Main function
    // ===========

    public static void main(String[] args) {
        if (args.length == 2) {
            NodeWrapper tree = new NodeWrapper(cacheDir + "/");
            try {
                File dir = new File(cacheDir);
                if (deleteDirectory(dir) && dir.mkdir() || dir.mkdir()) {
                    if (PatriciaTrie.treeBuilder(tree, new BufferedReader(new FileReader(args[0]))) != null) {
                        tree.write(new BufferedWriter(new FileWriter(args[1])));
                        deleteDirectory(dir);
                    }
                }
            } catch (Exception e) {
                e.getMessage();
            }
        } else {
            System.out.println("Usage: TestMiningCompiler /path/to/words.txt /path/to/dict.bin");
        }
    }

    public static boolean deleteDirectory(File directory) {
        if (directory.exists()) {
            File[] files = directory.listFiles();
            if (null != files) {
                for (int i = 0; i < files.length; i++) {
                    if (files[i].isDirectory()) {
                        deleteDirectory(files[i]);
                    } else {
                        files[i].delete();
                    }
                }
            }
        }
        return directory.delete();
    }
}
