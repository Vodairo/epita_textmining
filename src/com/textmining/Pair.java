package com.textmining;

/**
 * Created by Voda on 22/04/2015.
 */
public class Pair<F, S> {
    public F FFirst;
    public S FSecond;

    public Pair(F first, S second) {
        FFirst = first;
        FSecond = second;
    }
}
