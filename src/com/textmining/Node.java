package com.textmining;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

/**
 * Node class
 */
public class Node {

    // ===========
    // Constants
    // ===========

    private final static char[] voidString = {'\0'};

    // ===========
    // Variables
    // ===========

    protected Node FFather;
    protected LinkedList<Node> FSons;

    protected TMString FValue;
    protected int FOccurrence;

    // ===========
    // Getter/Setter
    // ===========

    public Node getFFather() {
        return FFather;
    }

    public void setFFather(Node FFather) {
        this.FFather = FFather;
    }

    public TMString getFValue() {
        return FValue;
    }

    public void setFValue(TMString FValue) {
        this.FValue = FValue;
    }

    public int getFOccurrence() {
        return FOccurrence;
    }

    public void setFOccurrence(int FOccurrence) {
        this.FOccurrence = FOccurrence;
    }

    public LinkedList<Node> getFSons() {
        return FSons;
    }

    // ===========
    // De/Constructor
    // ===========

    public Node(TMString value, int occurrence) {
        FFather = null;
        FSons = new LinkedList<Node>();

        FValue = value;
        FOccurrence = occurrence;
    }

    // ===========
    // Other methods
    // ===========

    /**
     * Add a son at its right place
     * @param son The son to be added
     * @return The son
     */
    public Node addSon(Node son) {
        boolean added = false;
        if (FSons.size() >= 1) {
            for (int i = 0; i < FSons.size(); i++) {
                if (son.FValue.inferiorTo(FSons.get(i).FValue)) {
                    FSons.add(i, son);
                    added = true;
                    break;
                }
            }
            if (!added)
                FSons.addLast(son);
        }
        else
            FSons.add(son);

        son.setFFather(this);

        return son;
    }

    /**
     * Add several sons using addSon method
     * @param sons The list of sons to be added
     */
    public void addSons(List<Node> sons) {
        for (Node son : sons) {
            this.addSon(son);
        }
    }

    /**
     * Split the value of a node and create a new son with the second part of the value
     * @param position The position where the split has to occur
     * @return The new son
     */
    public Node split(int position) {
        Node newNode = new Node(FValue.substring(position), FOccurrence);
        newNode.addSons(FSons);

        FSons.clear();
        FValue = FValue.substring(0, position);
        FOccurrence = 0;

        this.addSon(newNode);

        return newNode;
    }

    /**
     * Search the son where the Patricia Trie algorithm has to continue
     * @param value The actual value trying to be added in the tree
     * @return The son with the minimum distance with the actual value
     */
    public Pair<Node, Integer> searchInSons(TMString value) {
        int length = 0;
        if (FSons == null)
            return null;

        for (Node son : FSons) {
            while (true) {
                if (value.regionMatches(0, son.getFValue(), 0, length + 1))
                    length += 1;
                else if (length != 0)
                    return new Pair<Node, Integer>(son, length);
                else
                    break;
            }
        }

        return null;
    }

    /**
     * Basic write function, used to keep the progress of the algorithm
     * Write with the following syntax : (nodeName):(nodeOccurrence)[(sonsOfThisNode)]
     * @param bw Java class to write in the file
     * @throws Error when trying to write on the file
     */
    public void write(BufferedWriter bw) throws Exception {
        FValue.write(bw);
        bw.write(":" + FOccurrence);
        for (Node son : FSons) {
            bw.write("[" + System.lineSeparator());
            son.write(bw);
            bw.write("]");
        }
        bw.flush();
    }

    /**
     * Load a tree using one of the file used to keep progress
     * Same syntax as write method in Node
     * @param lastNode Father of the actual node
     * @param br Java class used to read the file
     * @return The actual node with all its sons
     * @throws Error when reading the file
     */
    public static Node load(Node lastNode, BufferedReader br) throws Exception {
        String line;
        final TMString standard = new TMString(voidString);

        Node n = new Node(standard, 0);


        while ((line = br.readLine()) != null) {
            for (int i = 0; i < line.length(); i++) {
                if (line.toCharArray()[i] == '[')
                    n = n.addSon(new Node(standard, 0));
                else if (line.toCharArray()[i] == ']') {
                    if (n.getFFather() != null)
                        n = n.getFFather();
                    else
                        n = lastNode;
                } else {
                    int index = line.indexOf(']');
                    if (index == -1) {
                        index = line.indexOf('[');
                        if (index == -1)
                            index = line.length();
                    }

                    String tmpLine = line.substring(0, index);
                    Pair<TMString, Integer> split = new TMString(tmpLine.toCharArray()).split(':');
                    n.setFValue(split.FFirst);
                    n.setFOccurrence(split.FSecond);

                    i = i + tmpLine.length() - 1;
                }
            }
        }

        return n;
    }

    /**
     * Iterate on the nodes given, and calculate the offset to the following node (used by write function of NodeWrapper)
     * @param nodes List of nodes on which to iterate
     * @return The string to be written on a file
     */
    public static String setOffsets(LinkedList<Node> nodes) {
        String firstPart = "";
        String secondPart;
        String thirdPart;
        long fourthPart;
        Node lastNode = null;
        for (Node node : nodes) {
            if (lastNode != null) {
                secondPart = new String(lastNode.FValue.getFValue()) + ":" + lastNode.getFOccurrence() + ";";
                if (!lastNode.getFSons().isEmpty())
                    thirdPart = "[" + setOffsets(lastNode.getFSons()) + "]";
                else
                    thirdPart = "";
                thirdPart += "][";
                fourthPart = secondPart.length() + thirdPart.length();
                int length = numberLength(fourthPart);
                fourthPart = length + fourthPart;
                if (length != numberLength(fourthPart))
                    fourthPart += 1;
                firstPart += secondPart + fourthPart + thirdPart;
            }
            lastNode = node;
        }

        if (lastNode != null) {
            if (!lastNode.getFSons().isEmpty())
                secondPart = "[" + setOffsets(lastNode.getFSons()) + "]";
            else
                secondPart = "";
            thirdPart = new String(lastNode.FValue.getFValue()) + ":" + lastNode.getFOccurrence() + ";";
            firstPart += thirdPart + "-" + secondPart;
        }

        return firstPart;
    }

    public static int numberLength(long number) {
        int result = 0;
        if (number < 0) {
            result += 1;
        }

        while (number != 0) {
            result += 1;
            number /= 10;
        }

        return result;
    }
}
