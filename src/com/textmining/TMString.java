package com.textmining;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Arrays;

/**
 * TMString class
 * Custom string wrapper to use less memory
 */
public class TMString {

    // ===========
    // Variables
    // ===========

    private char[] FValue;

    // ===========
    // Getter/Setter
    // ===========

    public char[] getFValue() {
        return FValue;
    }

    // ===========
    // De/Constructor
    // ===========

    public TMString(char value[]) {
        this.FValue = Arrays.copyOf(value, value.length);
    }

    public TMString(char[] value, int offset, int count) {
        if (offset < 0) {
            throw new StringIndexOutOfBoundsException(offset);
        }
        if (count < 0) {
            throw new StringIndexOutOfBoundsException(count);
        }
        // Note: offset or count might be near -1>>>1.
        if (offset > value.length - count) {
            throw new StringIndexOutOfBoundsException(offset + count);
        }
        this.FValue = Arrays.copyOfRange(value, offset, offset + count);

    }

    // ===========
    // Other methods
    // ===========

    public int length() {
        return FValue.length;
    }

    /**
     * Convert the string to integer if possible
     * @param beginIndex Index on which to begin the conversion
     * @param endIndex Index on which to end the conversion
     * @return The conversion value
     */
    public int toInteger(int beginIndex, int endIndex) {
        int result = 0;
        for (int i = beginIndex; i < endIndex; i++) {
            result = result * 10 + Character.getNumericValue(FValue[i]);
        }
        return result;
    }

    public Pair<TMString, Integer> split(char pattern) {
        for (int i = 0; i < FValue.length; i++) {
            if (FValue[i] == pattern) {
                return new Pair<TMString, Integer>(this.substring(0, i), this.toInteger(i + 1, FValue.length));
            }
        }

        return null;
    }

    public TMString substring(int beginIndex, int endIndex) {
        if (beginIndex < 0) {
            throw new StringIndexOutOfBoundsException(beginIndex);
        }
        if (endIndex > FValue.length) {
            throw new StringIndexOutOfBoundsException(endIndex);
        }
        int subLen = endIndex - beginIndex;
        if (subLen < 0) {
            throw new StringIndexOutOfBoundsException(subLen);
        }
        return ((beginIndex == 0) && (endIndex == FValue.length)) ? this
                : new TMString(FValue, beginIndex, subLen);
    }

    public TMString substring(int beginIndex) {
        if (beginIndex < 0) {
            throw new StringIndexOutOfBoundsException(beginIndex);
        }
        int subLen = FValue.length - beginIndex;
        if (subLen < 0) {
            throw new StringIndexOutOfBoundsException(subLen);
        }
        return (beginIndex == 0) ? this : new TMString(FValue, beginIndex, subLen);
    }

    public boolean regionMatches(int toffset, TMString other, int ooffset, int len) {
        char ta[] = FValue;
        int to = toffset;
        char pa[] = other.FValue;
        int po = ooffset;
        // Note: toffset, ooffset, or len might be near -1>>>1.
        if ((ooffset < 0) || (toffset < 0)
                || (toffset > (long) FValue.length - len)
                || (ooffset > (long) other.FValue.length - len)) {
            return false;
        }
        while (len-- > 0) {
            if (ta[to++] != pa[po++]) {
                return false;
            }
        }
        return true;
    }

    /**
     * Delete and return the first character
     * @return The first character
     */
    public Character popFirstChar() {
        char c = FValue[0];

        FValue = this.substring(1).getFValue();

        return c;
    }

    /**
     * Compare to string a return true if (this) is inferior to the other one
     * @param other The second string
     * @return Result of the comparison
     */
    public boolean inferiorTo(TMString other) {
        for (int i = 0; i < this.length() && i < other.length(); i++) {
            if (this.FValue[i] < other.FValue[i])
                return true;
            else if (this.FValue[i] > other.FValue[i])
                return false;
        }

        return this.length() < other.length();
    }

    /**
     * Write helper
     * @param bw Java class to write in a file
     */
    public void write(BufferedWriter bw) {
        try {
            bw.write(FValue);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
