package com.textmining;

import com.textmining.compiler.Main;

import java.io.*;
import java.util.*;

/**
 * NodeWrapper class
 * Wrapper to the class Node in order to use a pagination system
 * Used to save memory in the RAM
 */
public class NodeWrapper {

    public static Pair<Character, Node> actualNode = null;

    // ===========
    // Variables
    // ===========

    private final String FBasicPath;

    private HashMap<Character, File> FSerializedNode;

    // ===========
    // De/Constructor
    // ===========

    /**
     * Constructor to the class
     * @param basicPath The path where the dictionary is to be built
     */
    public NodeWrapper(String basicPath) {
        FBasicPath = basicPath;
        FSerializedNode = new HashMap<Character, File>();
    }

    // ===========
    // Other methods
    // ===========

    /**
     * Load the correct page (use serialize and deserialize methods)
     * @param actualPageID ID to right page
     * @return The page (as a reference to the root node)
     */
    public Node getPage(Character actualPageID) {
        if (actualNode == null || !actualNode.FFirst.equals(actualPageID)) {
            if (actualNode != null) {
                serialize();
            }

            if (FSerializedNode.containsKey(actualPageID)) {
                deserialize(actualPageID);
            } else {
                FSerializedNode.put(actualPageID, new File(FBasicPath + "_" + actualPageID + "_." + Main.cacheDir));

                char[] tmp = {actualPageID};

                actualNode = new Pair<Character, Node>(actualPageID, new Node(new TMString(tmp), 0));
            }
        }

        return actualNode.FSecond;
    }

    private void serialize() {
        File f = FSerializedNode.get(actualNode.FFirst);
        try {
            actualNode.FSecond.write(new BufferedWriter(new FileWriter(f)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deserialize(char actualPageID) {
        File f = FSerializedNode.get(actualPageID);
        try {
            actualNode = new Pair<Character, Node>(actualPageID, Node.load(null, new BufferedReader(new FileReader(f))));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Write the entire tree with the following syntax : (nodeValue):(nodeOccurrence);(offsetToTheNextBrother)[(sonsOfThisNode)]
     * nodeOccurrence and offsetToTheNextBrother are equal to '-1' when none is found
     * @param bw Java class to write in a file
     */
    public void write(BufferedWriter bw) throws Exception {
        serialize();

        String secondPart;
        String thirdPart;
        long fourthPart;
        Node lastNode = null;

        bw.write(":0;-[");
        for (char i = 0; i < 256; i++) {
            if (FSerializedNode.containsKey(i)) {
                deserialize(i);
                if (lastNode != null) {
                    secondPart = new String(lastNode.FValue.getFValue()) + ":" + lastNode.getFOccurrence() + ";";
                    if (!lastNode.getFSons().isEmpty())
                        thirdPart = "[" + Node.setOffsets(lastNode.getFSons()) + "]";
                    else
                        thirdPart = "";
                    thirdPart += "][";
                    fourthPart = secondPart.length() + thirdPart.length();
                    int length = Node.numberLength(fourthPart);
                    fourthPart = length + fourthPart;
                    if (length != Node.numberLength(fourthPart))
                        fourthPart += 1;
                    bw.write(secondPart + fourthPart + thirdPart);
                }
                lastNode = actualNode.FSecond;
            }
        }

        if (lastNode != null) {
            if (!lastNode.getFSons().isEmpty())
                secondPart = "[" + Node.setOffsets(lastNode.getFSons()) + "]";
            else
                secondPart = "";
            thirdPart = new String(lastNode.FValue.getFValue()) + ":" + lastNode.getFOccurrence() + ";";
            bw.write(thirdPart + "-" + secondPart);
        }

        bw.write("]");
        bw.flush();
    }

}
