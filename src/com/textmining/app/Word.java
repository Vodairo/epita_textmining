package com.textmining.app;

/**
 * Word class
 */
public class Word
{
    private String word;
    private int freq;
    private int distance;

    public Word(String word, int freq, int distance)
    {
        this.word = word;
        this.freq = freq;
        this.distance = distance;
    }

    public int getDistance()
    {
        return distance;
    }

    public int getFreq()
    {
        return freq;
    }

    public String getWord()
    {
        return word;
    }

    /**
     * @return A JSON format String representing the word
     */
    public String toJson()
    {
        return "{\"word\":\"" + word + "\",\"freq\":" + freq + ",\"distance\":" + distance + "}";
    }
}
