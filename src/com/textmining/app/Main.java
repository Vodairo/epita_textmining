package com.textmining.app;

import com.textmining.MappedTree;
import com.textmining.Pair;

import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * Created by Voda on 15/04/2015.
 */
public class Main
{
    public static void main(String[] args)
    {
        if (args.length == 1)
        {
            try
            {
                Scanner sc = new Scanner(System.in);
                String line = sc.nextLine();

                Corrector corrector = new Corrector(new MappedTree(args[0]));
                while (true)
                {
                    Pair<Integer, String> result_parse = parseLine(line);
                    if (result_parse != null)
                    {
                        int dis = result_parse.FFirst;
                        String word = result_parse.FSecond;

                        corrector.approx(word, dis);
                        corrector.printResultList();
                    }

                    try
                    {
                        line = sc.nextLine();
                    }
                    catch (NoSuchElementException e)
                    {
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else
            System.out.println("Usage: TextMiningApp /path/to/dict.bin");
    }

    /**
     * Parse a query
     * @param line The query as a String
     * @return A pair of integer and String representing the max distance and the word
     */
    private static Pair<Integer, String> parseLine(String line)
    {
        int i = 7;
        int j = i;

        try
        {
            while (line.charAt(j) != ' ')
                j++;

            Integer d = Integer.parseInt(line.substring(i, j));
            String word = line.substring(j + 1, line.length());
            return new Pair<Integer, String>(d, word);
        }
        catch (Exception e)
        {
            System.out.println("Command format: approx \"distance_max\" \"word\"");
        }
        return null;
    }
}