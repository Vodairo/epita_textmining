package com.textmining.app;

import com.textmining.MappedTree;

import java.util.ArrayList;

/**
 * Corrector class
 */
public class Corrector
{
    // Character array that represents the dictionary of word / frequency
    private MappedTree dictionary;
    // Result list of approximation
    private ArrayList<Word> resultList;
    // Indexes of resultList for each distances
    private int[] resultIndexes;
    // Matrix for computing Damerau-Levenshtein distance
    private int[][] DLTab;

    // Word to correct
    private String word;
    // Maximum distance
    private int disMax;

    public Corrector(MappedTree dictionary)
    {
        this.dictionary = dictionary;
        resultList = new ArrayList<Word>();
        resultIndexes = new int[0];
        DLTab = new int[0][0];
    }

    /**
     * Approximation function
     *
     * @param word The word to search
     * @param dis  The maximum distance
     */
    public void approx(String word, int dis) throws Exception
    {
        resultList = new ArrayList<Word>();
        resultIndexes = new int[dis + 1];
        for (int i = 0; i < dis; i++)
            resultIndexes[i] = 0;

        DLTab = new int[word.length() + dis + 1][word.length() + 1];
        for (int i = 0; i < DLTab.length; i++)
            DLTab[i][0] = i;
        for (int j = 0; j < DLTab[0].length; j++)
            DLTab[0][j] = j;

        this.word = word;
        disMax = dis;
        rApprox("");
    }

    /**
     * Print results with a JSON format
     */
    public void printResultList()
    {
        System.out.print("[");
        for (int i = 0; i < resultList.size(); i++)
        {
            System.out.print(resultList.get(i).toJson());
            if (i != resultList.size() - 1)
                System.out.print(",");
        }
        System.out.println("]");
    }

    /**
     * Recursive approximation function. DFS on a Patricia Trie.
     *
     * @param compareWord The actual word to compare
     */
    private void rApprox(String compareWord) throws Exception
    {
        int w2index = compareWord.length();
        compareWord += dictionary.getNodeValue();

        int realDis = DLDistance(compareWord, w2index);
        int minDis = getMinDis(compareWord.length());

        if (realDis <= disMax)
        {
            int currentOccurrence = dictionary.getNodeOccurrence();
            if (currentOccurrence != 0)
                addResultWord(compareWord, currentOccurrence, realDis);
        }

        if (minDis <= disMax && (compareWord.length() < DLTab.length || realDis < disMax))
        {
            if (dictionary.goToFirstSon())
            {
                do
                {
                    if (compareWord.length() + dictionary.getNodeValueLength() < DLTab.length)
                        rApprox(compareWord);
                }
                while (dictionary.goToNextBrother());
            }
        }
    }

    /**
     * Add a word to the result list
     *
     * @param word     The word to add
     * @param freq     The frequency of the word
     * @param distance The distance between the word to add and the word to search
     */
    private void addResultWord(String word, int freq, int distance)
    {
        Word rWord = new Word(word, freq, distance);
        int i = findIndex(rWord, 0, resultList.size());

        resultList.add(i, rWord);
    }

    /**
     * Find the index using a half-interval search to insert the new word in the result list
     *
     * @param rWord The word to insert
     * @param begin The begin index of the result list
     * @param end   The end index of the result list
     * @return An integer representing the index where to insert the new word
     */
    private int findIndex(Word rWord, int begin, int end)
    {
        if (resultList.isEmpty())
            return 0;
        if (end - begin <= 1)
        {
            if (rWord.getDistance() < resultList.get(begin).getDistance())
                return begin;
            else if (rWord.getDistance() > resultList.get(begin).getDistance())
                return end;
            else
            {
                if (rWord.getFreq() > resultList.get(begin).getFreq())
                    return begin;
                else if (rWord.getFreq() < resultList.get(begin).getFreq())
                    return end;
                else
                {
                    int c = compareWords(rWord.getWord(), resultList.get(begin).getWord());
                    if (c < 0)
                        return begin;
                    else
                        return end;
                }
            }
        }

        int i = begin + (end - begin) / 2;
        if (rWord.getDistance() < resultList.get(i).getDistance())
            return findIndex(rWord, begin, i);
        else if (rWord.getDistance() > resultList.get(i).getDistance())
            return findIndex(rWord, i, end);
        else
        {
            if (rWord.getFreq() > resultList.get(i).getFreq())
                return findIndex(rWord, begin, i);
            else if (rWord.getFreq() < resultList.get(i).getFreq())
                return findIndex(rWord, i, end);
            else
            {
                int c = compareWords(rWord.getWord(), resultList.get(i).getWord());
                if (c < 0)
                    return findIndex(rWord, begin, i);
                else
                    return findIndex(rWord, i, end);
            }
        }
    }

    /**
     * Tell if the word w1 is before the word w2 in lexicographical order
     *
     * @param w1 The first word to compare
     * @param w2 The second word to compare
     * @return An integer : -1 if w1 is before w2, 1 if w1 is after w2 and 0 if w1 equal w2
     */
    public int compareWords(String w1, String w2)
    {
        int i = 0;
        while (i < w1.length() && i < w2.length() && w1.charAt(i) == w2.charAt(i))

            i++;

        if (i == w1.length())
            return -1;
        if (i == w2.length())
            return 1;
        if (w1.charAt(i) < w2.charAt(i))
            return -1;
        if (w1.charAt(i) > w2.charAt(i))
            return 1;
        return 0;
    }

    /**
     * Computes a Damerau-Levenshtein distance between the attribute word and w2
     * beginning at w2Index for the second word and stores the results in DLTab
     *
     * @param w2      The second word
     * @param w2Index The index of the second word
     * @return A integer representing the distance
     */
    private int DLDistance(String w2, int w2Index)
    {
        for (int i = w2Index + 1; i < w2.length() + 1; i++)
        {
            for (int j = 1; j < DLTab[0].length; j++)
            {
                int c = 1;
                if (word.charAt(j - 1) == w2.charAt(i - 1)
                    || (i > 1 && j > 1 && word.charAt(j - 1) == w2.charAt(i - 2)
                    && word.charAt(j - 2) == w2.charAt(i - 1)))
                    c = 0;
                DLTab[i][j] = Math.min(Math.min(DLTab[i - 1][j - 1] + c, DLTab[i - 1][j] + 1), DLTab[i][j - 1] + 1);
            }
        }

        return DLTab[w2.length()][DLTab[0].length - 1];
    }

    /**
     * Get the minimum distance in a specific lign of DLTab
     *
     * @param w2Index The lign of DLTab
     * @return An integer representing the minimum distance
     */
    private int getMinDis(int w2Index)
    {
        int min = DLTab[w2Index][0];
        for (int i = 1; i < DLTab[0].length; i++)
        {
            if (min > DLTab[w2Index][i])
                min = DLTab[w2Index][i];
        }
        return min;
    }
}
