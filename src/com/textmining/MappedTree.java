package com.textmining;

import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.LinkedList;

/**
 * MappedTree class
 * Used as a minimalist wrapper to the FileChannel class
 */
public class MappedTree {

    // ===========
    // Variables
    // ===========

    private RandomAccessFile FFile;
    private FileChannel FFileChannel;

    private ByteBuffer FDictionary;

    private LinkedList<Integer> FFatherPosition;

    private int FLastPosition;

    // ===========
    // De/Constructor
    // ===========

    public MappedTree(String fileName) throws Exception {
        FFile = new RandomAccessFile(fileName, "r");

        FFileChannel = FFile.getChannel();
        FDictionary = FFileChannel.map(FileChannel.MapMode.READ_ONLY, 0, FFileChannel.size());

        FFatherPosition = new LinkedList<Integer>();
    }

    // ===========
    // Other methods
    // ===========

    /**
     * Return the value of the actual node
     * @return The value of the node
     * @throws Error with FileChannel class
     */
    public String getNodeValue() throws Exception {
        FLastPosition = FDictionary.position();

        String result = "";

        char c;
        while (true) {
            c = (char)FDictionary.get();
            if (c == ':')
                break;
            result += c;
        }

        FDictionary.position(FLastPosition);
        return result;
    }

    /**
     * Return the value of the actual node
     * @return The value of the node
     * @throws Error with FileChannel class
     */
    public int getNodeValueLength() throws Exception {
        FLastPosition = FDictionary.position();

        int result = 0;

        while (true) {
            if (FDictionary.get() == ':')
                break;
            result += 1;
        }

        FDictionary.position(FLastPosition);
        return result;
    }

    /**
     * Return the occurrence of the actual node
     * @return The occurrence of the node
     * @throws Error with FileChannel class
     */
    public int getNodeOccurrence() throws Exception {
        FLastPosition = FDictionary.position();

        int result = 0;

        for (; FDictionary.get() != ':';);

        byte b;
        while (true) {
            b = FDictionary.get();
            if (b == ';')
                break;
            result = result * 10 + Character.getNumericValue(b);
        }

        FDictionary.position(FLastPosition);
        return result;
    }

    /**
     * Set the FileChannel to the position of the first son of the actual node
     * @return If it was possible to reposition the FileChannel
     * @throws Error with FileChannel class
     */
    public boolean goToFirstSon() throws Exception {
        FLastPosition = FDictionary.position();

        char c;
        while (true) {
            c = (char)FDictionary.get();
            if (c == ']') {
                FDictionary.position(FLastPosition);
                return false;
            } else if (c == '[') {
                FFatherPosition.addLast(FLastPosition);
                return true;
            }
        }
    }

    /**
     * Set the FileChannel to the position of the next brother of the actual node
     * @return If it was possible to reposition the FileChannel
     * @throws Error with FileChannel class
     */
    public boolean goToNextBrother() throws Exception {
        FLastPosition = FDictionary.position();

        for (; FDictionary.get() != ';';);

        if (FDictionary.get(FDictionary.position()) == '-') {
            FDictionary.position(FFatherPosition.removeLast());
            return false;
        }

        int offset = 0;
        byte b;
        while (true) {
            b = FDictionary.get();
            if (b == '[' || b == ']')
                break;
            offset = offset * 10 + Character.getNumericValue(b);
        }

        FDictionary.position(FLastPosition + offset);
        return true;
    }

    /**
     * Reset the position to the origin
     * @throws Error with FileChannel class
     */
    public void reset() throws Exception {
        FDictionary.position(0);
    }
}
